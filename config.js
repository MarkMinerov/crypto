module.exports = {
    symbol: 'BTCUSDT',
    asset: 'BTC',
    quantity: 0.0003,
    coefficient: 0.005,
    resetOrdersTime: 60000 * 3,
    delayBetweenTrade: 60000 * 3,
    minimumIncome: 0.5,
    addIfMinimum: 0.8,
}