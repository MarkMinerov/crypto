const config = require('./config');
const Binance = require('node-binance-api');
const fs = require('fs');

let data = {
    bidPrice: null,
    askPrice: null,

    timer: null,
    lastOrderState: null,
    comission: null,
    lowestPrice: null,
    lastSellOrderDate: null,
    walletCheck: false,
};

const binance = new Binance().options({
    APIKEY: '4zrSy55LL2fM7EQ4KcGwy7DoCjI38VJFbUMe9TUNx6HQ2CREyDGfbZ2B40Bv4OTo',
    APISECRET: 'vhBFhxcUF6oedftHVOyJYaekistRAb63vMDQC72hqqlabz8tbkjxOe46jR9lbXsN',
    adjustForTimeDifference: true,
    verbose: true,
    recvWindow: 10000,
});

function main() {
    console.log('Начинаю трейдить...');

    binance.bookTickers(config.symbol, (error, ticker) => {
        if (!ticker || error) {
            console.log('Что-то пошло не так в момент запуска скрипта! Перезапускаюсь...');
            console.error(error);

            return setTimeout(main);
        }

        data.bidPrice = parseFloat(ticker.bidPrice);
        data.askPrice = parseFloat(ticker.askPrice);

        buyCryptoAndSell();
    });
}

function buyCryptoAndSell() {
    binance.balance((error, balances) => {
        if (error || balances.length === 0) {
            console.log('Что-то пошло не так в момент запуска скрипта! Перезапускаюсь...');
            console.error(error);

            return setTimeout(main);
        }

        console.log('Проверка на уже существующий ордер...');

        binance.openOrders(false, (error, openOrders) => {
            if (error) {
                console.log('Что-то пошло не так! Перезапускаюсь...');
                console.error(error);

                resetData();
                return setTimeout(main);
            }

            const firstOrder = openOrders[0];

            // При запуске скрипта проверка на то, продается ли крипта или покупается уже в момент исполнения скрипта 
            if (firstOrder && firstOrder.side) {
                data.lastOrderState = firstOrder.side;
                
                data.lastSellOrderDate = Date.now();
                startWatchingOrders();
            }
            
            // если выставленных товаров нет
            else {
                // получаем наличие нужной нам валюты
                const USDT = parseInt(balances.USDT.available);

                console.log(data.bidPrice * config.quantity);

                // если наших денег меньше чем сумма для покупки нужного значения бит валюты
                if (USDT < data.bidPrice * config.quantity) {
                    console.log('Нет денег для покупки крипты, но возможно крипта уже есть');

                    // Ставим глобальную проверку кошелька, так как получить кол-во нужной нам бит валюты мы не можем, по этому сразу пробуем просто продать бит валюту
                    // если продаст, значит у нас она была, если нет, значит и денег нет, тогда ставим скрипт в ожидание пополнение кошелька
                    data.walletCheck = true;

                    return countPriceAndSell();
                }

                // TODO: На замену
                // ========================================
                binance.buy(config.symbol, config.quantity, data.bidPrice, { type:'LIMIT' }, (error, response) => {
                    if (error) {
                        console.log('Что-то пошло не так при покупке! Перезапускаюсь!');
                        console.error(error ? error.body : 'no-error');

                        resetData();
                        return setTimeout(main);
                    }
        
                    // Цена за один коин
                    const pricePerOne = parseFloat(response.price);
        
                    data.lastOrderState = 'BUY';
        
                    //Это минимальная цена, за которую можно продать
                    data.lowestPrice = pricePerOne;
        
                    startWatchingOrders();
                });
                // ========================================
            }
        });
    });
}

function startWatchingOrders() {
    binance.openOrders(false, async (error, openOrders) => {
        if (error) {
            console.log(timestamp, 'Что-то пошло не так! Перезапускаюсь...');
            resetData();

            return setTimeout(main);
        }

        if (openOrders.length) {
            const currentTime = Date.now();

            let timestamp = new Date();
            timestamp = `[${ timestamp.getHours() }:${ timestamp.getMinutes() }:${ timestamp.getSeconds() }]`;

            if (openOrders[0].side === 'BUY') {
                clearLastLine();
                console.log(timestamp, 'Ожидается покупка крипты типа:', config.symbol);

                data.lastOrderState = 'BUY';

                // если никто не купил валюту через определенное время
                if (currentTime - data.lastSellOrderDate >= config.resetOrdersTime) {
                    console.log(timestamp, 'Никто не продаёт вам валюту... Перезапускаю продажу...');

                    clearTimeout(data.timer);
                    resetData();

                    binance.cancelAll(config.symbol)
                    .catch(err => {
                        console.log(err.body);
                    })
                    .finally(main);
                }
            }

            else if (openOrders[0].side === 'SELL') {
                clearLastLine();

                console.log(timestamp, 'Ожидается продажа крипты типа: ', config.symbol);

                // если кто-то не купил валюту через определенное время
                if (currentTime - data.lastSellOrderDate >= config.resetOrdersTime) {
                    console.log(timestamp, 'Никто не покупает вашу валюту... Перезапускаю продажу...');

                    clearTimeout(data.timer);

                    binance.bookTickers(config.symbol, (error, ticker) => {
                        if (!ticker) {
                            console.log(timestamp, 'Что-то пошло не так в момент запуска скрипта! Перезапускаюсь...');
                            return setTimeout(main);
                        }
                
                        data.bidPrice = parseFloat(ticker.bidPrice);
                        data.askPrice = parseFloat(ticker.askPrice);
                
                        binance.cancelAll(config.symbol)
                        .catch(err => {
                            console.log(timestamp, err.body);
                        })
                        .finally(countPriceAndSell);
                    });
                }
            }
        }

        else {
            if (data.lastOrderState === 'BUY') {                
                console.log('Кто-то продал тебе крипт-валюту! Теперь надо продавать!');

                return countPriceAndSell();
            }

            else {
                console.log('Кто-то купил твою крипт-валюту, перезапускаю скрипт для продолжения торговли..');
                console.log(openOrders);

                resetData();
                return setTimeout(main, config.delayBetweenTrade);
            }
        }
    });

    data.timer = setTimeout(startWatchingOrders, 5000);
}

function countPriceAndSell() {
    binance.trades(config.symbol, (error, trades, symbol) => {
        if (!trades.length || error) {
            console.log('Что-то пошло не так, перезапускаю скрипт...', error);

            resetData();
            return setTimeout(main);
        }

        const lastTrade = trades.reverse()[0];

        binance.allOrders(config.symbol, (error, orders, symbol) => {
            if (error) {
                console.log('Что-то пошло не так, перезапускаю скрипт...', error);

                resetData();
                return setTimeout(main);
            }

            const lastBuyPrice = parseFloat(orders.reverse()[orders.findIndex(v => v.side === 'BUY')].price);

            let price = lastBuyPrice + (data.askPrice * config.coefficient);
            const realQuantity = config.quantity - lastTrade.commission;

            while ((price * realQuantity - lastBuyPrice * realQuantity) < config.minimumIncome) {
                // Цена продажи слишком мала, надо добавить стоимости!
                price += ((price * realQuantity + config.addIfMinimum) / realQuantity) - price;
            }

            clearTimeout(data.timer);
            startSelling(price, lastTrade.commission);
        });
    });
}

function startSelling(price, commission) {
    console.log('Могу продать за', price.toFixed(5));

    // реальное количество товара с учётом комиссии
    const realQuantity = (config.quantity - commission).toString().slice(0, 4);

    binance.sell(config.symbol, realQuantity, price.toFixed(4))

    .then(() => {
        data.lastOrderState = 'SELL';

        // если купило, значит у нас были деньги для этого, и проверку на состояние кошелька можно отменить
        data.walletCheck = false;

        // записываем время после покупки для проверок
        data.lastSellOrderDate = Date.now();

        // ставим таймер для проверки оредеров
        data.timer = setTimeout(startWatchingOrders, 5000);
    })
    // одна из ошибок: недостача денег на балансе
    .catch(err => {
        console.log(err.body, realQuantity, price);

        // если тут брала участие глобальная проверка на наличие денег, то делать следущие действия
        if (data.walletCheck) {
            console.log('Нет денег для покупки валюты! Перезапускаюсь..');

            resetData();
            return setTimeout(main);
        }

        console.log('Произошла ошибка с приобретением валюты, перезапускаю скрипт...');

        resetData();
        setTimeout(main);
    });
}

function resetData() {
    clearTimeout(data.timer);

    data = {
        bidPrice: null,
        askPrice: null,
    
        timer: null,
        lastOrderState: null,
        comission: null,
        lowestPrice: null,
        lastSellOrderDate: null,
        walletCheck: false,
    };
}

/* 
* смысл этой функции в том, чтобы проверить, не выросла ли крипт валюта если выросла
* с момента последнего закупа, мы будем ждать, пока валюта упадет до примерно той же цены,
* за которую был последний закуп, это уменшит риски ухода в минус, если цена сначала выросла,
* и скрипт закупил, а потом резко упала, и скрипт уже никак не сможет продать товар за нужную нам сумму
*/
function watchPriceThenBuy() {
    // если последняя цена покупки не была записана до этого
    if (data.lowestPrice == null) {
        console.log('Прошлая цена покупки не зарегистрирована! Регистрирую...');

        return binance.allOrders(config.symbol, (error, orders, symbol) => {
            // Нам нужны только закупочные ордеры
            const buyOrders = orders.reverse().filter(el => el.side === 'BUY');

            if (error) {
                console.log('Что-то пошло не так! Перезапускаюсь...', error ? error.body : null);

                resetData();
                return setTimeout(main);
            }

            // Если ещё не приходилось покупать валюту такого типа
            else if (buyOrders.length === 0) {
                console.log('Вам ещё не приходилось покупать валюту данного типа, по этому делаю закуп по bid цене...');
                
                clearTimeout(data.timer);
                return makeBuyOrder();
            }

            const lastBuyPrice = buyOrders[0].price;
            data.lowestPrice = lastBuyPrice;
        });
    } 
    
    // если у нас уже записаны данные о последней цене закупа крипт-валюты
    else {
        binance.bookTickers(config.symbol, (error, ticker) => {
            if (!ticker || error) {
                console.log('Что-то пошло не так в момент запуска скрипта! Перезапускаюсь...');
                console.error(error);
    
                return setTimeout(main);
            }
    
            // data.bidPrice = parseFloat(ticker.bidPrice);
            // data.askPrice = parseFloat(ticker.askPrice);
    
            // buyCryptoAndSell();

            data.timer = setTimeout(watchPriceThenBuy, 5000);
        });
    }
}

function makeBuyOrder() {
    binance.buy(config.symbol, config.quantity, data.bidPrice, { type:'LIMIT' }, (error, response) => {
        if (error) {
            console.log('Что-то пошло не так при покупке! Перезапускаюсь!', erorr.body);

            resetData();
            return setTimeout(main);
        }

        // Цена за один коин
        const pricePerOne = parseFloat(response.price);

        data.lastOrderState = 'BUY';

        //Это минимальная цена, за которую можно продать
        data.lowestPrice = pricePerOne;

        startWatchingOrders();
    });
}

function clearLastLine() {
    process.stdout.moveCursor(0, -1);
    process.stdout.clearLine(1);
}

main();
// watchPriceThenBuy();